## basic db usage (without Entity Framework)

normally I use EF (pretty much 100 %) and you should also always use EF in your projects. 
However, if you are stuck doing things manually, then this code should help you 

---

This project shows how to manually connect to a local db

It also shows how to create a new table. 

It also shows how to query a table and get a specific row. 

the connection string can be changed to connect to a online server. 

The only thing is, when connecting online, Initial Cataglog needs to be set. 

for local db, there is no initial catalog. 

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 

