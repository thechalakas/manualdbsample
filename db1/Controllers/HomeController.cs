﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace db1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //lets create the table. 
            CheckLocalDBConnection();

            return View();
        }

        public void CheckLocalDBConnection()
        {

            //connection string for online database
            //connectionString="Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=aspnet-MvcMovie;Integrated Security=SSPI;AttachDBFilename=|DataDirectory|\Movies.mdf"
            //connectionSTring = @"Data Source = <server address>; Initial Catalog=<database name>; Persistent Security Info = True; User ID = <user id>; Password = <password her>";

            //connectiong string for local database. NOTE : There is no Initial Catalog. 
            var connectionString = @"Data Source=(LocalDb)\MSSQLLocalDB;Integrated Security=true;AttachDBFilename=|DataDirectory|\Database1.mdf";
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();

            //SqlCommand sqlCommand = new SqlCommand("CREATE TABLE Persons (PersonID int,FirstName varchar(255))");
            //            / INSERT INTO Customers(CustomerName, ContactName, Address, City, PostalCode, Country)
            //VALUES('Cardinal', 'Tom B. Erichsen', 'Skagen 21', 'Stavanger', '4006', 'Norway');
            SqlCommand sqlCommand = new SqlCommand("INSERT INTO Persons(PersonID,FirstName) VALUES(2,'Jay');");
            sqlCommand.Connection = Con;
            var response =  sqlCommand.ExecuteNonQuery();


            //// Open the connection
            //conn.Open();

            //// 1. Instantiate a new command with a query and connection
            //SqlCommand cmd = new SqlCommand("select CategoryName from Categories", conn);

            //// 2. Call Execute reader to get query results
            //rdr = cmd.ExecuteReader();

            //// print the CategoryName of each record
            //while (rdr.Read())
            //{
            //    Console.WriteLine(rdr[0]);
            //}

            var conn = Con;
            SqlCommand cmd = new SqlCommand("select FirstName from Persons", conn);

            // 2. Call Execute reader to get query results
            var rdr = cmd.ExecuteReader();
            var numberofrows = 0;
            var listofnames = new List<string>();
            // print the CategoryName of each record
            while (rdr.Read())
            {

                var temp = rdr[0].ToString();
                listofnames.Add(temp);
                numberofrows++;
            }

            var totalrows = numberofrows;
            var finalnames = listofnames;

            Con.Close();

        }
    }
}
